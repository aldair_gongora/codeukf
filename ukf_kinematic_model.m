% Aldair Gongora
% Kinematic Model 
clear all
close all
clc 
%% Preliminaries: 
% Process Noise and Covariance Matrix 
% VECTOR
% eta_g  - gyro instrumentation error? 
% eta_a  - accelerometer instrumentation error? 
% eta_ab - accelerometer bias (gaussian white noise)
% eta_wb - gyro bias (gausian white noise) 
% eta    - process noise vector 
eta_g  = [1 1 1]; 
eta_a  = [1 1 1]; % Vector? 
eta_ab = [1 1 1];
eta_wb = [1 1 1]; 
eta    = [eta_g' ; eta_a' ; eta_ab' ; eta_wb']; 
% Covariance Matrix 
% Q_g   - Covariance of gyro instrumentation error? 
% Q_a   - Covariance of accelerometer instrumentation error? 
% Q_ab  - Covariance of accelerometer bias 
% Q _bw - Covariance of gyro bias  
Q_g  = [1 1 1]; 
Q_a  = [1 1 1]; 
Q_ba = [1 1 1]; 
Q_bw = [1 1 1]; 
Q    = [Q_g Q_a Q_ba Q_bw];
Q    = diag(Q); 
% Time Step 
dt = 0.1; 
%% Collected Data 
% a - raw measurement of acceleration 
a = [1,1,1,1,1,1,1,1,1]; 
% w - raw measurement of angular velocity
w = [1,1,1,1,1,1,1,1,1];
%% Navigation State - x: b_a,b_w, gamma_gravity
% Bias
% b_a - acceleration bias 
b_a(:,1) = 1; % Initialize acceleration bias 
b_a(:,k + 1) = b_a(:,k) + dt * eta_ba; 
% b_w - gyro bias 
b_w(:,1) = 1; % Initialize angular velocity bias 
b_w(:,k+1) = b_w(:,k) + dt* eta_bw; 
% Gravity 
% gamma_gravity - gravity vector 
gamma_gravity = 9.81; % m/s^2  
% Rotation Matrix
% R_C_W - Rotation of the frame W with respect to the frame C 
R_C_W = [ ; ; ]; 
%% IMU Measurements as Control Inputs
% IMU Acceleration
% a_m - model measurement of acceleration 
a_m(:,k) = R_C_W*(a(:,k)- gamma(:,k)) + b_a(:,k) + eta_a(:,k); 
% IMU Angular Velocity 
% w_m - model measurement of angular velocity  
w_m(:,k) = w(:,k) + b_w(:,k) + eta_w(:,k);
%% Navigation State - x: v 
% Velocity 
% v - velocity 
v(:,1) = 0; % Initialize Velocity  
v(:,k+1) = v(:,k) + dt * a_m(:,k);
%% Discrete Time Evolution of the System 
x_se3(:,k) = ;

%% Vision 







