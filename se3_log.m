function g_dot_1 = se3_log(g1,g2)
%Description: 
g_2_1 = g1\g2;
[w,v] = se3_log_identity(g_2_1); % w_2_1; v_2_1 
twist_hat = RT2G(hat3(w),v);
twist_hat(4,4) = 0; 
g_dot_1 = g1* twist_hat;
end