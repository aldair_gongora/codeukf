function g_2 = se3_exp(g_1,g_dot_1)
%Description: 
%   Description of parameters. 
%transform tangent vector at g into tangent vector at identity by left
%translation
twist_hat = g_1\g_dot_1; 
g_2_1 = se3_exp_identity(twist_hat); % Homogenous transform of 2 relative to 1
g_2 = g_1*g_2_1; % Homogenous transform of 2 relative to the world frame 
%% OLD CODE
% R_1=G2R(g_1); % R1 from g_1
% R_dot_1=G2R(g_dot_1); % R_dot_1 
% T_dot_1=G2T(g_dot_1); % T_dot_1 
% 
% omega_hat_1=R_1'*R_dot_1; % w_hat_1 
% 
% % Twist at the identity 
% g_twist_identity= RT2G(omega_hat_1,T_dot_1); % Extracting w_hat_1 and T_dot. 
% % Shouldn't this be g_2? 
% g_exp_identity= se3_exp_identity(g_twist_identity);
% % If g_exp_identity is g_2, then rename and remove the step below. 
% g_2 = g_1*g_exp_identity;
end
