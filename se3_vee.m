function [v,w] = se3_vee(g,g_twist)
%Description: The following code
%   Detailed explanation ...
R = G2R(g); 
w_hat=G2R(g_twist);
w_hat = [g_twist(1,1) g_twist(1,2) g_twist(1,3);g_twist(2,1) g_twist(2,2) g_twist(2,3);g_twist(3,1) g_twist(3,2) g_twist(3,3)];
v = G2T(g_twist); 
v = [g_twist(1,4);g_twist(2,4);g_twist(3,4)];
w = rot_vee(R,w_hat); 
end

