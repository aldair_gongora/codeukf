function [x_update,P_0] = ukf_Kalman_update_state_covar(x_sigma_mean,K_gain,y_data,y_sigma_mean,x_sigma_covar,y_sigma_covar)
%Implementation of the Unscented Kalman Filter
% Update State Estimate 
x_update = x_sigma_mean' + K_gain*(y_data - y_sigma_mean'); 
% Update Error Covariance 
P_0 = x_sigma_covar - K_gain*y_sigma_covar*K_gain';
end

