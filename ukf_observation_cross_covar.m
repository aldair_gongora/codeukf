function [P_xy] = ukc_observation_cross_covar(L,eta_covar,x_sigma,x_sigma_mean,y_sigma,y_sigma_mean)
%Implementation of the Unscented Kalman Filter
%   Calculation of the cross covariance of the state and the output 
P_xy = zeros(L,2);
for i = 1:2*L+1
    P_xy = P_xy + eta_covar(i)*(x_sigma(:,i)-x_sigma_mean')*(y_sigma(:,i) - y_sigma_mean')';
    
end
end

