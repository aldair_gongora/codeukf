function [w,v] = se3_log_identity(g_2_1)
%Description: The following code
%   Given g, produce w_1 and v_1. 
R = G2R(g_2_1); % R_2_1
T = G2T(g_2_1); % T_2_1

w = vee3(rot3_log(R));
theta = norm(w);
%V_inv = eye(3,3) - 0.5*hat3(w) + (theta - sin(theta)/theta.^3)*hat3(w)*hat3(w); 
V_inv = eye(3,3) - 0.5*hat3(w) + (1/(theta^2))*(1 - ((theta*sin(theta))/(2*(1-cos(theta)))))*(hat3(w)*hat3(w)); 
v = V_inv*T; % v_1
end