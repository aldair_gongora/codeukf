function [x_sigma_mean,x_sigma_covar] = ukf_prediction_mean_covar(L,eta_mean,x_sigma,Q,varargin)
%Implementation of the Unscented Kalman Filter
%   Calulation of the mean and covariance of the predicted state. 

method='accumulate';

%optional parameters
ivarargin=1;
while ivarargin<=length(varargin)
    switch lower(varargin{ivarargin})
        case 'method'
            ivarargin=ivarargin+1;
            method=lower(varargin{ivarargin});
        otherwise
            error(['Argument ' varargin{ivarargin} ' not valid!'])
    end
    ivarargin=ivarargin+1;
end


% Mean of the Predicted State 
x_sigma_mean = eta_mean*x_sigma'; % Row Vector 1X4 
switch method
    case 'accumulate'
        % Covariance of the Predicted State 
        P_m = Q; 
            for i = 1:2*L+1
                P_m = P_m + eta_mean(i)*(x_sigma(:,i)-x_sigma_mean')*(x_sigma(:,i) - x_sigma_mean')';
            end 
        % Covariance of the Predicted State 
        x_sigma_covar = P_m;
    case 'sum'
        P_m = zeros(4); 
        for i = 1:2*L+1
            P_m = P_m + eta_mean(i)*(x_sigma(:,i)-x_sigma_mean')*(x_sigma(:,i) - x_sigma_mean')';
        end
        x_sigma_covar = Q + P_m; 
end
% % Alternate Method Check (Verified) 
% diff = x_sigma_covar - P_m_2


end

