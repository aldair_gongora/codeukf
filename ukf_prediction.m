function [ sigma_points ] = ukf_prediction_sigmapoints(P,x,L,lambda)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
sigma_points = [x, x*ones(1,L)+sqrt(L+lambda)*chol(P,'lower'), x*ones(1,L)-sqrt(L+lambda)*chol(P,'lower')];
end

