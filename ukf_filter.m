function [y_sigma_mean_data,y_sigma_covar_data] = ukf_filter(dt,time,observation_function,obsv_parameter_1,system_function,Q,R,initial_conditions)
%Implementation of the Unscented Kalman Filter
%   Detailed explanation goes here
%% PRELIMINARIES
% Time 
t = 0:dt:(time-dt);
t = t';
time_steps = time/dt; 
% Zero-mean process noise
diag_size_Q = sqrt(numel(Q)); 
w_k = sqrt(Q)*randn(diag_size_Q,time_steps); % Random process noise for each time step 
% Zero-mean measurement noise 
diag_size_R = sqrt(numel(R)); 
v_k = sqrt(R)*randn(diag_size_R,time_steps); % Random measurement noise for each time step 
%% PARAMETERS
% Primary scaling factor 
alpha = 1;
% Secondary scaling factor 
beta = 2; 
% Tertiary scaling parameter 
k = 0; 
% Dimension of state vector 
%L = sqrt(numel(f));  
L = 4; 
%% INITIALIZE 
% Initializing output vector 
% Size is the number of elements in the initial conditions vector
size = numel(initial_conditions); 
x = zeros(size,time_steps); 
% Initial conditions 
x(:,1) = initial_conditions;
IC = x(:,1); 
% Initial error matrix 
P_0 = eye(size,size); 
%% SIMULATING OUTPUT DATA 
[x_data_sim,y_data_sim] = ukf_simulation_output(L,time_steps,Q,R,IC,obsv_parameter_1,P_0,system_function,observation_function);
%% WEIGHT VECTORS FUNCTION CALL 
[eta_mean,eta_covar,lambda]=ukf_weightvectors(alpha,beta,k,L);
%% Prediction, Observation, and Update Loop 
j =1; 
m = 2; 
for k = 2:time_steps
    %% Prediction Step 
    % Generate Sigma Points
    sigma_points =  ukf_prediction_sigmapoints(P_0,x(:,k-1),L,lambda);
    % Propagate Sigma Points through f
    x_sigma = ukf_prediction_propagate(system_function,sigma_points);
    % Mean and Covariance of Predicted State 
    [x_sigma_mean,x_sigma_covar] = ukf_prediction_mean_covar(L,eta_mean,x_sigma,Q);
    %% Observation Step 
    % Propagate Sigma Points through Output g  
    [y_sigma] = ukf_observation_propagate(observation_function,obsv_parameter_1,L,x_sigma); 
    % Mean and Covariance of Predicted Output 
    [y_sigma_mean,y_sigma_covar] = ukf_observation_mean_covar(L,eta_mean,y_sigma,R,eta_covar);
    % Cross Covariance of State and Output 
    [P_xy] = ukf_observation_cross_covar(L,eta_covar,x_sigma,x_sigma_mean,y_sigma,y_sigma_mean);
    %Record Mean and Covariance of Predicted Output 
    y_sigma_mean_data(:,k-1) = y_sigma_mean;
    y_sigma_covar_data(j:m,:) = y_sigma_covar; 
    j = j + 2;
    m = m + 2;   
    %% Measurement Update 
    % Kalman Gain 
    [K_gain] = ukf_kalman_gain(P_xy,y_sigma_covar);
    % State Estimate Update and Error Covariance Update 
    [x_update,P_0] = ukf_kalman_update_state_covar(x_sigma_mean,K_gain,y_data_sim(:,k),y_sigma_mean,x_sigma_covar,y_sigma_covar); 
    x(:,k) = x_update;
end
%% DISPLAY RESULTS 
figure_num = 1; 
ukf_state_plot(L,t,x_data_sim,x,figure_num)
end

