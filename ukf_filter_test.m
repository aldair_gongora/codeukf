function [] = ukf_filter_test(dt,time,process_noise,meas_noise,initial_conditions)
% Aldair E. Gongora 
% UKF Implementation 
% February 20, 2017 
% For reference parameters, open 'ukf_test.m' 
%% PRELIMINARIES FROM USER ENTERED PARAMETERS
% Process Noise Covariance 
Q = diag(process_noise); 
% Measurement Noise Covariance 
R = diag(meas_noise); 
%%  OBSERVATION AND SYSTEM FUNCTIONS 
% Enter the observation and system functions along with any parameters. 
% Tracking Station Location 
N_1 = 20;
E_1 = 0;
N_2 = 0; 
E_2 = 20; 
tstation_coord = [N_1,E_1,N_2,E_2]';
% Observation Function
observation_function =@(x,obsv_parameter_1) [sqrt((x(1)-obsv_parameter_1(1))^2+(x(2)-obsv_parameter_1(2))^2); sqrt((x(1)-obsv_parameter_1(3))^2+(x(2)-obsv_parameter_1(4))^2)];
% System State Function 
system_function = @(x)([1, 0, dt, 0;0 1 0 dt;0 0 1 0; 0 0 0 1]*x);
%% UKF Filter 
% Format: ukf_filter(dt,time,observation_function,obsv_parameter_1,system_function,Q,R,initial_conditions)
ukf_filter(dt,time,observation_function,tstation_coord,system_function,Q,R,initial_conditions);
end