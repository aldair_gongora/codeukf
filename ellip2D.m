function [X,Y] = ellip2D(x,P,steps)
% Unscented Transform Error Ellipse 
%   Detailed explanation goes here
% Calculate "standard deviation" matrix
P = sqrtm(P);
% Determine rotation angle of ellipse
T = 0.5*atan(-2*P(1,2)/(P(1,1)^2-P(2,2)^2));
% Pre-calculate trigonometric functions for ellipse rotation
sinbeta = sin(T);
cosbeta = cos(T);
% Calculate equatorial radii of ellipse - major and minor 
a = sqrt(P(1,1)^2*sin(T)^2+2*P(1,2)*sin(T)*cos(T)+P(2,2)^2*cos(T)^2);
b = sqrt(P(1,1)^2*sin(T+pi/2)^2+2*P(1,2)*sin(T+pi/2)*cos(T+pi/2)+P(2,2)^2*cos(T+pi/2)^2);
a = abs(a);
b = abs(b);
% Generate points along the ellipse 
alpha = linspace(0, 360, steps)' .* (pi / 180); % Pre-calculate trigonometric functions for ellipse points
sinalpha = sin(alpha);
cosalpha = cos(alpha);
% Calculate Cartesian (x,y) coordinates of ellipse points
X = x(1) + (a * cosalpha * sinbeta + b * sinalpha * cosbeta);
Y = x(2) + (a * cosalpha * cosbeta - b * sinalpha * sinbeta);
end

