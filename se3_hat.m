function g_dot_1 = se3_hat(g_1,w_1,v_1)
%Description: The following code produces g_dot_1 which is the tangent
%vector based on g_1, w_1, and v_1. 
%   g_1 is a homogenous transform. 
%   w_1 is the twist coordinate of the twist
%   v_1 is the twist coordinates of the twist 
N=size(w_1,2);
R=G2R(g_1);
g_dot_1=[rot_hat(R,w_1) v_1; zeros(1,4,N)];
%T = G2T(g_1); 
%w_hat_1 = hat3(w_1); 
%g_dot_1 = [rot_hat(R,w_1) w_hat_1*T+ v_1; zeros(1,4,N)]; 
end