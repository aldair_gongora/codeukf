function [ sigma_points ] = ukf_prediction_sigmapoints(P,x,L,lambda)
%Implementation of the Unscented Kalman Filter
%   Calculation of sigma points for each time step 
sigma_points = [x, x*ones(1,L)+sqrt(L+lambda)*chol(P,'lower'), x*ones(1,L)-sqrt(L+lambda)*chol(P,'lower')];
end

