function [x_data_sim,y_data_sim] = ukf_simulation_output(L,time_steps,Q,R,IC,tstation_coord,P_0,system_function,observation_function)
%Implementation of the Unscented Kalman Filter
%   Generation of simulation output data 
% Process Noise 
w = sqrt(Q)*randn(L,time_steps);
% Measurement Noise 
v = sqrt(R)*randn(2,time_steps); 
% State Vector x 
x_data_sim = zeros(4,time_steps); 
% Initial Conditions 
x_data_sim(:,1) = IC+sqrt(P_0)*randn(4,1);
% Output Matrix 
y = zeros(2,time_steps) ;

for k = 2:time_steps 
    %x_data_sim(:,k) = f*x_data_sim(:,k-1) + w(:,k-1); 
    x_data_sim(:,k) = system_function(x_data_sim(:,k-1)) + w(:,k-1);
    y(:,k) = observation_function(x_data_sim(:,k-1),tstation_coord)+ v(:,k); 
    %y(:,k) = [sqrt((x_data_sim(1,k)-tstation_coord(1))^2+(x_data_sim(2,k)-tstation_coord(2))^2); ...
        %sqrt((x_data_sim(1,k)-tstation_coord(3))^2+(x_data_sim(2,k)-tstation_coord(4))^2)] + v(:,k);   
end
y_data_sim = y;
    


end

