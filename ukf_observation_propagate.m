function [y_sigma] = ukf_observation_propagate(observation_function,tstation_coord,L,x_sigma)
%Implementation of the Unscented Kalman Filter
%   Propagation of sigma points through the g function (observation) 
for i = 1:2*L+1   
    y_sigma(:,i) = observation_function(x_sigma(1:2,i),tstation_coord);
    %y_sigma(:,i) = [sqrt((x_sigma(1,i)-tstation_coord(1))^2+(x_sigma(2,i)-tstation_coord(2))^2); sqrt((x_sigma(1,i)-tstation_coord(3))^2+(x_sigma(2,i)-tstation_coord(4))^2)];   
end 

end


