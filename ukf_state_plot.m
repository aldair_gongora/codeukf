function [] = ukf_state_plot(L,t,x_data_sim,x,figure_num)
%Implementation of the Unscented Kalman Filter
%   Plotting of the states 
figure(figure_num)
for i = 1:L
    subplot(4,2,2*i-1); plot(t,x_data_sim(i,:),'b-', t,x(i,:),'g-.','LineWidth', 2);
    xlabel('Time (s)'); ylabel(['x_',num2str(i)]); grid on; legend('UKF','True (Simulated)');
    subplot(4,2,2*i); plot(t,x(i,:)-x_data_sim(i,:),'b-','LineWidth', 2);
    xlabel('Time (s)'); ylabel(['\Deltax_',num2str(i)]); grid on; legend('UKF Deviation');
    axis([0,60,-50,50]) 
end

    


end

