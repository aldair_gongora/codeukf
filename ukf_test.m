% Aldair E. Gongora 
% UKF Implementation 
% February 20, 2017 
clear all
close all
clc
%% PRELIMINARIES FROM USER ENTERED PARAMETERS
% Time 
dt = 0.1;
time = 100; 
% Process Noise Covariance 
Q = blkdiag(0,0,4,4); 
% Measurement Noise Covariance 
R = blkdiag(1,1); 
% Initial Condtions 
initial_conditions = [0,0,50,50];
%%  OBSERVATION AND SYSTEM FUNCTIONS 
% Enter the observation and system functions along with any parameters. 
% Tracking Station Location 
N_1 = 20;
E_1 = 0;
N_2 = 0;  
E_2 = 20; 
tstation_coord = [N_1,E_1,N_2,E_2]';
% Observation Function
observation_function =@(x,obsv_parameter_1) [sqrt((x(1)-obsv_parameter_1(1))^2+(x(2)-obsv_parameter_1(2))^2); sqrt((x(1)-obsv_parameter_1(3))^2+(x(2)-obsv_parameter_1(4))^2)];
% System State Function 
system_function = @(x)([1, 0, dt, 0;0 1 0 dt;0 0 1 0; 0 0 0 1]*x);
%% UKF Filter 
% Format: ukf_filter(dt,time,observation_function,obsv_parameter_1,system_function,Q,R,initial_conditions)
[y_sigma_mean_data,y_sigma_covar_data] = ukf_filter(dt,time,observation_function,tstation_coord,system_function,Q,R,initial_conditions);
%% Error Ellipse 
m = 1;
figure(2); clf
for i = 1:990 
    [Xu, Yu] = ellip2D(y_sigma_mean_data(:,i),y_sigma_covar_data(m:m+1,:),151);
    m = m + 2; 
    plot(Xu,Yu,'r.',y_sigma_mean_data(1,i),y_sigma_mean_data(2,i),'r*');
    hold on
    pause 
end   
