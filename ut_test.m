% Aldair E. Gongora 
% March 15, 2017 
clear all
clc
%% NonLinear Function 
system_function = @(x) [x(1)^2 + x(2)^2; x(1)*x(2)]; % Case 1
%system_function = @(x) [(x(1)^4)*x(2); x(1)+ x(2)^5]; % Case 2
%% Prior Conditions 
x = [0.1;-1]; 
P = (10^(-2))*[8 2; 2 3]; 
%% UKF Preliminaries 
alpha = 0.5; 
beta = 2; 
k = 0; 
L = 2; 
% Weight Vectors 
[eta_mean,eta_covar,lambda] = ukf_weightvectors(alpha,beta,k,L);
% Sigma Points 
sigma_points =  ukf_prediction_sigmapoints(P,x,L,lambda);
% Propagate Sigma Points 
for k = 1: 2*L+1
[y_sigma] = ukf_prediction_propagate(system_function,sigma_points(:,k));
y_sigma_total(:,k) = y_sigma;
end
% Mean and Covar Statistics 
P_check = [0 0; 0 0]; 
[y_sigma_mean,y_sigma_covar] = ukf_prediction_mean_covar(L,eta_mean,y_sigma_total,P_check);
%% EKF 
% EKF Approximations
ye = [x(1)^2 + x(2)^2; x(1)*x(2)]; % Case 1
A = [2*x(1) 2*x(2); x(2) x(1)]; % Case 1 
%ye = [(x(1)^4)*x(2); x(1)+ x(2)^5]; % Case 2
%A = [4*x(1)^3*x(2) x(1)^4;1 5*x(2)^4]; % Case 2
Pe = A*P*A';
%% Monte Carlo Simulation 
% Monte Carlo Simulation
nsim = 1e6;
R = chol(P); 
z = repmat(x',nsim,1) + randn(nsim,2)*R; 
z = z';
ysv = zeros(2,nsim);
for i = 1:nsim
ysv(:,i) = [z(1,i)^2 + z(2,i)^2; z(1,i)*z(2,i)]; % Case 1
%ysv(:,i) = [z(1,i)^4*z(2,i); z(1,i)+ z(2,i)^5]; % Case 2
end
ys = mean(ysv,2);
Ps = cov(ysv');
%%
% Create Ellipses based on mean and covariance
[Xe, Ye] = ellip2D(ye,Pe,151);
[Xu, Yu] = ellip2D(y_sigma_mean,y_sigma_covar,151);
[Xs, Ys] = ellip2D(ys,Ps,151);
[X0, Y0] = ellip2D(x,P,151);
%[Xuk, Yuk] = ellip2D(yu,Pu,151); 

%% Individual Plots 
figure() 
plot(X0,Y0,'k-',x(1),x(2),'ko');
xlabel('x_1')
ylabel('x_2')
%legend('Prior Cov','Prior Mean');
grid on
%title('Mean and Covariance Before Nonlinear Transformation y = f(x)'); 

figure()
plot(Xe,Ye,'b.',ye(1),ye(2),'bs')
hold on
plot(Xu,Yu,'r.',y_sigma_mean(1),y_sigma_mean(2),'r*')
hold on
plot(Xs,Ys,'k-',ys(1),ys(2),'ko')
hold off 
grid on
xlabel('y_1')
ylabel('y_2')
%title('Mean and Covariance After Nonlinear Transformation y = f(x)'); 
%legend('EKF Cov','EKF Mean','UKF Cov','UKF Mean','True Cov', 'True Mean');
%% SubPlots 
% Plot Results
figure; subplot(121); plot(X0,Y0,'k-',x(1),x(2),'ko');
xlabel('x_1'); ylabel('x_2'); legend('Prior Cov','Prior Mean');
grid on
subplot(122);
plot(Xe,Ye,'b.',ye(1),ye(2),'bs'); hold on; 
plot(Xu,Yu,'r.',y_sigma_mean(1),y_sigma_mean(2),'r*'); hold on
%plot(Xuk,Yuk,'g.',yu(1),Pu(2),'bs'); hold on
plot(Xs,Ys,'k-',ys(1),ys(2),'ko'); hold off 
grid on
xlabel('y_1'); ylabel('y_2'); 
title('Mean and Covariance After Nonlinear Transformation y = f(x)'); 
legend('EKF Cov','EKF Mean','UKF Cov','UKF Mean','True Cov', 'True Mean');
%legend('EKF Cov','EKF Mean','UKF Cov','UKF Mean','Test1','Test2','True Cov', 'True Mean');

%% RMSE 
% Calculate RMSE
% Xs, Ys - Monte Carlo "True" 
% Xe, Ye - EKF Output 
% Xu, Yu - UKF Output 
XYs(1,:) = Xs; 
XYs(2,:) = Ys; 
XYe(1,:) = Xe; 
XYe(2,:) = Ye; 
XYu(1,:) = Xu; 
XYu(2,:) = Yu; 

%display('ekf_rmse')
ekf_rmse = sqrt(mean(sum(XYs(:,:)-XYe(:,:)).^2))
%display('ukf_rmse')
ukf_rmse = sqrt(mean(sum(XYs(:,:)-XYu(:,:)).^2))
%display('ekf_rmse_mean')
ekf_rmse_mean = sqrt(mean(sum(ys(:,:)-ye(:,:)).^2))
%display('ukf_rmse_mean')
ukf_rmse_mean =  sqrt(mean(sum(ys(:,:)-y_sigma_mean(:,:)').^2))
%ekf_rmse = sqrt(mean(sum((X(1:2,:)-MM_EKF(1:2,:)).^2)));
%ME_EKF = squeeze(PP_EKF(1,1,:)+PP_EKF(2,2,:));
%fprintf('Done! Also, calculated RMSE!\n')
%MM_EKF_Unfiltered = MM_EKF;

%fprintf('EKF-RMSE   = %.6f [%.6f]\n',ekf_rmse,sqrt(mean(ME_EKF)));

%%
% UKF Approximations
% alpha = 0.5; % Define primary scaling parameter
% beta = 2;% Define secondary scaling parameter (Gaussian)
% L = 2;% Number of states
% lambda = L*(alpha^2-1);
% eta = sqrt(L + lambda);
% wm = zeros(2*L+1,1); wc = zeros(2*L+1,1);
% wm(1) = lambda/(L + lambda);
% wc(1) = lambda/(L + lambda) + (1 - alpha^2 + beta);
% for i = 2:length(wm)
%     wm(i) = 1/(2*(L + lambda));
%     wc(i) = 1/(2*(L + lambda));
% end
% sP = sqrtm(P); 
% chi = [x x*[1 1]+eta*sP x*[1 1]-eta*sP];
% ysig = zeros(2,2*L+1);
% for i = 1:(2*L+1)
%     ysig(:,i) = [chi(1,i)^2 + chi(2,i)^2; chi(1,i)*chi(2,i)];
% end
% yu = ysig*wm;
% Pu = zeros(2,2); % Check 
% for i = 1:(2*L+1)
%     Pu = Pu + wc(i)*(ysig(:,i)-yu)*(ysig(:,i)-yu)';
% end