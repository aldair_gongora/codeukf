function [y_sigma_mean,y_sigma_covar] = ukf_observation_output_mean_covar(L,eta_mean,y_sigma,R,eta_covar)
%Implementation of the Unscented Kalman Filter
%   Calculation of output Mean and Covariance 
% Output Mean 
y_sigma_mean = eta_mean*y_sigma';
% Output Covariance 
P_m = R; 
    for i = 1:2*L+1
        P_m = P_m + eta_mean(i)*(y_sigma(:,i)-y_sigma_mean')*(y_sigma(:,i) - y_sigma_mean')';
    end 
% Covariance of the Predicted State 
y_sigma_covar = P_m;

end

