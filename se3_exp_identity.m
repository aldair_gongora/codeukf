function g_2_1 = se3_exp_identity(twist_hat)
[w_hat_1, v_1] = G2RT(twist_hat);
w_1 = vee3(w_hat_1); 
R_2_1 = rot3_exp(w_hat_1); 
theta=norm(w_1);
if theta > 0 
    V=eye(3)+(1-cos(theta))/theta^2*hat3(w_1)+(theta-sin(theta))/theta^3*hat3(w_1)^2;
    T_2_1=V*v_1;
else
    disp('Division by zero (Norm of w is zero)')
    R_2_1 = eye(3); 
    T_2_1 = v_1; 
end
g_2_1 = RT2G(R_2_1,T_2_1);
%% OLD CODE
% omega_hat_identity = G2R(g_twist_identity);
% omega_identity = vee(omega_hat_identity);
% R = rot_exp(eye(3),omega_hat_identity);
% v_identity = G2T(g_twist_identity);
% if norm(omega_identity) > 0 
%     T=((eye(3)-R)*(omega_hat_identity)*v_identity + omega_identity*omega_identity'*v_identity)/norm(w);
% else
%     error('Division by zero (Norm of omega is zero)')
% end
% g_2_1 = RT2G(R,T);