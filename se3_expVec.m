function g_2 = se3_expVec(g_1,w_1,v_1)
%Description: The following codes produces g_2 based on three inputs: g_1,
%w_1, and v_1. 
%   g_1 is the homogenous transform 
%   w_1 and v_1 are twist coordinates
%   g_2 is the homogenous transform to be solved for based on g_1,w_1, and
%   v_1. 
g_dot_1 = se3_hat(g_1,w_1,v_1);
g_2 = se3_exp(g_1,g_dot_1); 
end