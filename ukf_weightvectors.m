function [eta_mean,eta_covar,lambda] = ukf_weightvectors(alpha,beta,k,L)
%Implementation of the Unscented Kalman Filter
%   The following code calculates the weight vectors chi_mean and chi_covar
lambda = alpha^2*(L+k)-L;
eta_mean = []; 
eta_mean = lambda/(L+lambda);
for i = 2:2*L+1
eta_mean(i) =  1/(2*(L+lambda));
end 
eta_covar = []; 
eta_covar = lambda/(L+lambda) + 1 - alpha^2 + beta; 
for i = 2:2*L+1
eta_covar(i) =  1/(2*(L+lambda));
end

end

