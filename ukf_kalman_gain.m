function [K_gain] = ukf_kalman_gain(P_xy,y_sigma_covar)
%Implementation of the Unscented Kalman Filter
%   Calculation of the Kalman Gain 
K_gain = P_xy*inv(y_sigma_covar);
end

