function [ x_sigma] = ukf_prediction_propagate(system_function,sigma_points)
%Implementation of the Unscented Kalman Filter
%   The sigma points are propagated through the state function. 
x_sigma = system_function(sigma_points);
%x_sigma = f*sigma_points; 
end

